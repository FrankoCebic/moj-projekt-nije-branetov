var express = module.require('express');
var app = express();
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}));
app.use(express.static(__dirname + '/static'));

var secret = "neka-fraza";
var users=[
  {email: "a", password: "a"}
];

app.listen(3000, function(){
	console.log("listening on 3000");
});



///////////////////////////////


app.post('/auth',function(req,res){
  var status = 401;
  var response = {"success": false};

  var user = getUser(req.body.email, req.body.pass);
  if(user != null) {
    // var token = jwt.sign(user, secret {
    //  expiresIn: '1m'
    //});
    var token = jwt.sign(user, secret);
    response.success = true;
    response.token =token;
    status = 200;
  }
  res.status(status).json(response);
});


app.post('/register', function(req, res){
  var email = req.body.email;
  var password = req.body.pass;
  var success = false;

  if(!userExists(email)){
    var user = {"email": email, "password": password}
    users.push(user);
    success=true;
  }

  var status = success ? 200: 400;
  res.status(status).json({"success":success});
});


///////////////////////////////

var apiRoute = express.Router();


apiRoute.use(function(req, res, next){
  console.log("use function");
  var token = req.headers['x-auth-token'];
  if(token) {
    jwt.verify(token, secret, function(err, payload){
      if(err) {
        return res.status(400).json({success:false, message:"krivi token"}); 
      } else{
        next();
      }
    })
  } else {
    return res.status(400).json({success:false, message:"fali token"})
  }
});

apiRoute.get('/users', function(req, res) {
  res.status(200).json(users);
});

app.use('/api', apiRoute);
////////////////////////////////////
function getUser(email, pass) {
  for(var i=0; i<users.length; i++){
    if(users[i].email == email && users[i].password == pass){
      return users[i];
    }
    return null;
  }
}

function verifyLogin(email, pass){
  console.log(email, pass)
  console.log(users)
	for(var i=0; i<users.length; i++){
		if(users[i].email==email && users[i].password==pass){
			return true;
		}
	}
	return false;
}

function userExists(email){
 for(var i=0; i<users.length; i++){
    if(users[i].email==email){
      return true;
    }
  }
  return false;
}
