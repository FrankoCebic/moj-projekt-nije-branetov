var myApp = angular.module('projekt');

myApp.controller('LoginController', function($rootScope ,$scope, $http){
  var self = this;
  this.email = "prazno";
  this.pass = "prazno";
  $rootScope.loginSuccess = false;

  this.send = function(email, pass){
    self.email = email;
    self.pass = pass;


    var data = {email:email, pass:pass};
    $http({
      data:data,
      method:'POST',
      url:'/auth'
    }).then(function successCallback(response){
      console.log("response", response);
      $rootScope.loginSuccess = true;
      $rootScope.token = response.data.token;
    }, function errorCallback(response){
      console.log("greska");
      $rootScope.loginSuccess = false;
    });

  };

});